module github.com/EbonJaeger/mcsmanager

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/DataDrake/cli-ng v1.0.1
	github.com/DataDrake/waterlog v1.0.5
	github.com/dustin/go-humanize v1.0.0
	github.com/stretchr/stew v0.0.0-20130812190256-80ef0842b48b
	github.com/stretchr/testify v1.4.0 // indirect
)
